import sys


def main(input):
    floor = 0
    for x in input:
        if x == '(':
            floor += 1
        else:
            floor -= 1
    print floor

if __name__ == "__main__":
    main(sys.stdin.readline())
