import sys
import re


def main(input):
    numbers = re.findall(r'-?\d+', input)
    answer = 0
    for num in numbers:
        answer += int(num)
    print(answer)

if __name__ == "__main__":
    main(sys.stdin.read().strip())
