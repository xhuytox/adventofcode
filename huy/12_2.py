import re


def calc_total(input):
    numbers = re.findall(r'-?\d+', input)
    total = 0
    for number in numbers:
        total += int(number)
    return total


def find_start_brace_index(input, index):
    open_parens = 0
    for i in xrange(index, 0, -1):
        if input[i] == '{' and open_parens == 0:
            return i
        elif input[i] == '{':
            open_parens -= 1
        elif input[i] == '}':
            open_parens += 1

    return -1


def find_end_brace_index(input, index):
    open_parens = 0
    for i in xrange(index, len(input)):
        if input[i] == '}' and open_parens == 0:
            return i
        elif input[i] == '}':
            open_parens -= 1
        elif input[i] == '{':
            open_parens += 1

    return -1

file = open('12.input', 'r')
input = file.read()
index = input.find(':"red"')
while index > 0:
    start_brace_index = find_start_brace_index(input, index)
    if start_brace_index < 0:
        print('Error: no start index was found')
        continue

    end_brace_index = find_end_brace_index(input, index)
    if end_brace_index < 0:
        print('Error: no end index was found')
        continue

    input = input[:start_brace_index] + input[end_brace_index + 1:]
    index = input.find(':"red"')

print(calc_total(input))
