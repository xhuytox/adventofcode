class Reindeer:

    def __init__(self, name='', speed=0, run_time=0, rest_time=0):
        self.name = name
        self.speed = speed
        self.run_time = run_time
        self.rest_time = rest_time
        self.points = 0

    def calc_distance(self, time):
        cycle_time = self.run_time + self.rest_time
        cycle_num = int(time / cycle_time)
        cycle_distance = self.run_time * self.speed
        rem = time % cycle_time
        distance = cycle_num * cycle_distance
        additional = 0
        if rem < self.run_time:
            additional = rem * self.speed
        else:
            additional = cycle_distance
        distance += additional
        return distance


def award_points(deers, time, distance):
    for deer in deers:
        if deer.calc_distance(time) == distance:
            deer.points += 1


f = open('14.input', 'r')
input = f.readlines()

deers = []
for line in input:
    words = line.split()
    deer = Reindeer()
    deer.name = words[0]
    deer.speed = int(words[3])
    deer.run_time = int(words[6])
    deer.rest_time = int(words[13])
    deers.append(deer)

for time in xrange(1, 2504):
    max_distance = 0
    for deer in deers:
        distance = deer.calc_distance(time)
        if distance > max_distance:
            max_distance = distance
    award_points(deers, time, max_distance)

for deer in deers:
    print(deer.name)
    print(deer.points)
