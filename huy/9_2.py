import sys
import itertools


def main(input):
    ref = build_refence(input)
    dist_ref = ref[0]
    cities = ref[1]
    longest = -sys.maxint - 1
    longest_path = None
    for path in itertools.permutations(list(cities)):
        distance = calculate(list(path), dist_ref)
        if distance > longest:
            longest = distance
            longest_path = list(path)
    print longest_path
    print longest


def build_refence(input):
    dist_ref = {}
    cities = set()
    for line in input:
        words = line.split()
        cityA = words[0]
        cityB = words[2]
        distance = int(words[4])
        dist_ref[cityA + cityB] = distance
        dist_ref[cityB + cityA] = distance
        cities.add(cityA)
        cities.add(cityB)
    return (dist_ref, cities)


def calculate(path, dist_ref):
    distance = 0
    for i in xrange(len(path) - 1):
        cityA = path[i]
        cityB = path[i + 1]
        distance += dist_ref[cityA + cityB]
    return distance


if __name__ == "__main__":
    main(sys.stdin.readlines())
