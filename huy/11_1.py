import sys
import re


def main(input):
    string = input
    while True:
        string = increment_string(string)
        print(string)
        if (check_straight(string) and
                check_iol(string) and
                check_doubles(string)):
            break

        if len(re.findall(r'^[a]+$', string)) > 0:
            break


def check_straight(string):
    for i in xrange(0, len(string) - 2):
        char = string[i]
        if string[i + 1] == chr(ord(char) + 1):
            if string[i + 2] == chr(ord(char) + 2):
                return True
    return False


def check_iol(string):
    if ('i' in string or
            'o' in string or
            'l' in string):
        return False
    return True


def check_doubles(string):
    if len(re.findall(r'(\w)\1', string)) > 1:
        return True


def increment_string(string):
    reversed_string = list(string[::-1])
    for i in xrange(0, len(reversed_string)):
        if reversed_string[i] == 'z':
            reversed_string[i] = 'a'
        else:
            reversed_string[i] = chr(ord(reversed_string[i]) + 1)
            return ''.join(reversed_string)[::-1]
    return ''.join(reversed_string)[::-1]


if __name__ == "__main__":
    main(sys.stdin.read().strip())
