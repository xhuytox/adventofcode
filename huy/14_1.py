class Reindeer:

    def __init__(self, name='', speed=0, run_time=0, rest_time=0):
        self.name = name
        self.speed = speed
        self.run_time = run_time
        self.rest_time = rest_time

    def calc_distance(self, time):
        cycle_time = self.run_time + self.rest_time
        cycle_num = int(time / cycle_time)
        cycle_distance = self.run_time * self.speed
        rem = time % cycle_time
        distance = cycle_num * cycle_distance
        additional = 0
        if rem < self.run_time:
            additional = rem * self.speed
        else:
            additional = cycle_distance
        distance += additional
        return distance


f = open('14.input', 'r')
input = f.readlines()

deers = []
for line in input:
    words = line.split()
    deer = Reindeer()
    deer.name = words[0]
    deer.speed = int(words[3])
    deer.run_time = int(words[6])
    deer.rest_time = int(words[13])
    deers.append(deer)

time = 2503
max_distance = 0
farthest_deer = ''
for deer in deers:
    distance = deer.calc_distance(2503)
    if distance > max_distance:
        max_distance = distance
        farthest_deer = deer.name

print(farthest_deer)
print(max_distance)
