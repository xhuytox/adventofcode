import sys


def main(input):
    visited = {(0, 0): 2}
    x1 = 0
    y1 = 0
    x2 = 0
    y2 = 0
    santa = True
    for direction in input:
        if direction == '>':
            if santa:
                x1 += 1
            else:
                x2 += 1
        elif direction == '<':
            if santa:
                x1 -= 1
            else:
                x2 -= 1
        elif direction == '^':
            if santa:
                y1 += 1
            else:
                y2 += 1
        elif direction == 'v':
            if santa:
                y1 -= 1
            else:
                y2 -= 1

        if santa:
            coordinate = (x1, y1)
        else:
            coordinate = (x2, y2)

        if coordinate in visited:
            visited[coordinate] += 1
        else:
            visited[coordinate] = 1

        santa = not santa

    print len(visited)

if __name__ == "__main__":
    main(sys.stdin.readline())
