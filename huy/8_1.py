import sys
import re


def main(input):
    string = "".join(input.split())
    total_count = len(string)

    total_quote_count = len(re.findall(r'"', string))
    escaped_quote_count = len(re.findall(r'\\\"', string))
    no_escape_quote_count = total_quote_count - escaped_quote_count

    escaped_backslash_count = len(re.findall(r'\\\\', string))
    escaped_ascii_count = len(re.findall(r'\\x([0-9a-f]){2}', string))

    string_value_count = total_count  \
        - no_escape_quote_count \
        - escaped_quote_count \
        - escaped_backslash_count \
        - (3 * escaped_ascii_count)

    print total_count - string_value_count

if __name__ == "__main__":
    main(sys.stdin.read())
