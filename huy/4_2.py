import sys
import hashlib


def main(input):
    i = 1
    while True:
        s = input + str(i)
        md5 = hashlib.md5(s).hexdigest()
        if md5[0:6] == '000000':
            print i
            break
        i += 1

if __name__ == "__main__":
    main(sys.stdin.readline().strip('\r\n'))
