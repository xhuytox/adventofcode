import sys
import copy


booklet = {}


def main(input):
    global booklet
    for line in input:
        connection = line.split('->')
        lhs = connection[0].strip()
        rhs = connection[1].strip()
        if isdigit(lhs):
            booklet[rhs] = int(lhs)
        else:
            booklet[rhs] = lhs

    booklet_temp = copy.deepcopy(booklet)
    a = solve_wire('a')
    booklet_temp['b'] = a
    booklet = copy.deepcopy(booklet_temp)
    print solve_wire('a')


def solve_wire(wire):
    global booklet
    # Base case 1
    if isdigit(wire):
        return int(wire)

    # Base case 2
    rhs = booklet[wire]
    if isdigit(rhs):
        return int(rhs)

    instruction = rhs.split()

    # Direct wire
    if len(instruction) == 1:
        wireA = instruction[0]
        booklet[wire] = solve_wire(wireA)
        return booklet[wire]

    # NOT
    if len(instruction) == 2:
        wireA = instruction[1]
        booklet[wire] = ~solve_wire(wireA)
        return booklet[wire]

    op = instruction[1]
    wireA = instruction[0]
    wireB = instruction[2]

    if op == 'OR':
        booklet[wire] = solve_wire(wireA) | solve_wire(wireB)
        return booklet[wire]
    elif op == 'AND':
        booklet[wire] = solve_wire(wireA) & solve_wire(wireB)
        return booklet[wire]
    elif op == 'RSHIFT':
        booklet[wire] = solve_wire(wireA) >> int(wireB)
        return booklet[wire]
    elif op == 'LSHIFT':
        booklet[wire] = solve_wire(wireA) << int(wireB)
        return booklet[wire]


def isdigit(value):
    try:
        int(value)
        return True
    except ValueError:
        return False


if __name__ == "__main__":
    main(sys.stdin.readlines())

