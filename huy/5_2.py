import sys


def main(input):
    count = 0
    for name in input:
        letter_pair_twice = False
        alternating_letter = False

        for i in xrange(0, len(name) - 3):
            if name[i:i+2] in name[i+2:len(name)]:
                letter_pair_twice = True
                break

        for i in xrange(0, len(name) - 2):
            if name[i] == name[i+2]:
                alternating_letter = True
                break

        if letter_pair_twice and alternating_letter:
            count += 1

    print count

if __name__ == "__main__":
        main(sys.stdin.readlines())
