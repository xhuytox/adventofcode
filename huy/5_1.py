import sys
import re


def main(input):
    count = 0
    for name in input:
        three_vowels = False
        double_letter = False
        no_invalid_sub = True

        vowels = re.findall(r'[aeiou]', name)
        if len(vowels) > 2:
            three_vowels = True

        if re.search(r'([a-z])\1', name):
            double_letter = True

        invalid_subs = ['ab', 'cd', 'pq', 'xy']
        for subs in invalid_subs:
            if subs in name:
                no_invalid_sub = False
                break

        if three_vowels and double_letter and no_invalid_sub:
            count += 1

    print count

if __name__ == "__main__":
        main(sys.stdin.readlines())
