import sys


def main(input):
    pos = 1
    floor = 0
    for x in input:
        if x == '(':
            floor += 1
        else:
            floor -= 1

        if floor < 0:
            print pos
            break

        pos += 1

if __name__ == "__main__":
    main(sys.stdin.readline())
