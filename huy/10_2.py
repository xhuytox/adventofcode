import sys


def main(input):
    new_string = str(input.strip())
    for i in xrange(0, 50):
        new_string = look_and_say(new_string)
    print len(new_string)


def look_and_say(string):
    curr_digit = string[0]
    curr_quantity = 1
    new_string = ''
    for digit in string[1:]:
        if digit == curr_digit:
            curr_quantity += 1
        else:
            new_string += str(curr_quantity) + curr_digit
            curr_digit = digit
            curr_quantity = 1
    new_string += str(curr_quantity) + curr_digit
    return new_string


if __name__ == "__main__":
    main(sys.stdin.readline())
