import sys
import itertools


def build_ref(list):
    dict = {}
    names = set()
    for line in list:
        words = line.split()
        name1 = words[0]
        name2 = words[10].rstrip('.')

        modifier = words[2]
        if modifier == 'gain':
            amount = int(words[3])
        else:
            amount = int("-" + words[3])

        dict[name1 + name2] = amount
        names.add(name1)
        names.add(name2)
    return (dict, names)


def calc_happiness(happiness_ref, arrangement):
    happiness = 0
    for i in xrange(len(arrangement) - 1):
        name1 = arrangement[i]
        name2 = arrangement[i+1]
        happiness += happiness_ref[name1 + name2]
        happiness += happiness_ref[name2 + name1]

    name1 = arrangement[0]
    name2 = arrangement[-1]
    happiness += happiness_ref[name1 + name2]
    happiness += happiness_ref[name2 + name1]
    return happiness

file = open('13.input', 'r')
input = file.readlines()

ref = build_ref(input)
happiness_ref = ref[0]
names_ref = ref[1]
max_happiness = -sys.maxint - 1
for arrangement in itertools.permutations(list(names_ref)):
    happiness = calc_happiness(happiness_ref, arrangement)
    if happiness > max_happiness:
        max_happiness = happiness
        best_arrangement = arrangement
        print(arrangement)
        print(happiness)

print(best_arrangement)
print(max_happiness)
