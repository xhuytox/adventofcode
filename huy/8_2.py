import sys


def main(input):
    trimmed_input = []
    new_input = []
    for string in input:
        trimmed_input.append(string.strip())
        new_input.append(encode(string.strip()))
    print len("".join(new_input)) - len("".join(trimmed_input))


def encode(s):
    result = ''
    for c in s:
        if c == '"':
            result += "\\\""
        elif c == '\\':
            result += "\\\\"
        else:
            result += c
    print s.strip()
    print result.strip()
    return '"' + result + '"'

if __name__ == "__main__":
    main(sys.stdin.readlines())
